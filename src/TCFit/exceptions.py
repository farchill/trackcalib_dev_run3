class ValidationError(Exception):
    """Is raised whenever an error occurs which is associated
    with validation
    """
    pass